import type { NextPage } from 'next';
import Head from 'next/head';
import HomeLayout from 'src/modules/home/HomeLayout';
import { Box } from '@mui/material';

const HomePage: NextPage = () => {
  return (
    <div>
      <Head>
        <title>Mamelkecha  | Resume Builder</title>
        <meta name="description" content="Single Page Resume Builder" />
        
      </Head>
     
      <HomeLayout />
    </div>
  );
};

export default HomePage;
