import Link from 'next/link';
import Image from 'next/image';
import { Box } from '@mui/material';
import stand from '../../../../public/stand.png'
import success from '../../../../public/success.png'
import skill from '../../../../public/skill.png'
import focus from '../../../../public/focus.png'

function FeatureSection() {
  return (
    <>
 
      <FeatureCard>
        <CardPinnnedIcon>
          <Image src={stand} alt="stand out" height="56px" width="56px" />
        </CardPinnnedIcon>
        <p className="text-xl mr-14">
          Makes <strong>you stand out among the crowd</strong>!
        </p>
      </FeatureCard>

      <FeatureCard>
        <CardPinnnedIcon>
          <Image src={skill} alt="speed" height="56px" width="56px" />
        </CardPinnnedIcon>
        <p className="text-xl mr-14">
          Clearly showcase your <strong>skills & qualities!</strong>
        </p>
      </FeatureCard>

      <FeatureCard>
        <CardPinnnedIcon>
          <Image src={success} alt="magic" height="56px" width="56px" />
        </CardPinnnedIcon>
        <p className="text-xl mr-14">
          Illustrates <strong>your accomplishments </strong>well!
        </p>
      </FeatureCard>

      <FeatureCard>
        <CardPinnnedIcon>
          <Image src={focus} alt="lock" height="56px" width="56px" />
        </CardPinnnedIcon>
        <p className="text-xl mr-14">
          Describes how much attention you give to <strong>little details!</strong>
        </p>
      </FeatureCard>
    </>
  );
}

const FeatureCard = ({ children }: { children: React.ReactNode }) => {
  return (
    <Link href="/builder" passHref={true}>
      <div
        className={`transition ease-in-out delay-100 duration-300 bg-resume-100 hover:bg-resume-500 text-resume-800
      hover:text-resume-50 fill-resume-800 px-6 py-10 lg:p-12 flex shadow-md cursor-pointer relative rounded-xl`}
      >
        {children}
      </div>
    </Link>
  );
};

const CardPinnnedIcon = ({ children }: { children: React.ReactNode }) => {
  return (
    <div className="backdrop-blur-2xl bg-resume-100 rounded-full p-2 shadow-level-hard absolute right-0 top-0 -mt-4 -mr-1">
      {children}
    </div>
  );
};

export default FeatureSection;
